import Foundation
import SLog

class DefaultMessageConverter: TemplatedMessageConverter {
    static let instance: TemplatedMessageConverter = DefaultMessageConverter()

    private init() { }

    func convertToString(templated: String, arguments: [TypeWrapperRepresentable]) -> String {
        String(format: templated, arguments: arguments.map { $0.typeWrappedValue.description })
    }
}
