// swift-tools-version:5.3

import PackageDescription

let package = Package(
    name: "SLogFileBackend",
    products: [
        .library(
            name: "SLogFileBackend",
            targets: ["SLogFileBackend"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/aylook-public/slog/slog.git", from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "SLogFileBackend",
            dependencies: [
                .product(name: "SLog", package: "slog")
            ])
    ]
)
